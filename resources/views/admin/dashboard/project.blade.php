@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Projeto</h1>
  </div>

  <div class="section-body">
    <div class="row">
      <div class="col-6 col-md-6 col-lg-6">
        <div class="card dashboard-funil-full">
          <div class="card-header">
            <h4>
              <i class="far fa-star lga"></i> Ícones
            </h4>
          </div>
          <div class="card-body row">
              {!! Form::model($data, ['route' => 'adm.project.store', 'files' => true]) !!}
              {!! Form::label('icon', 'Seção 1') !!}
              <br>
              {!! Form::file('icon1') !!}
              <br>
              Descrição: {!! Form::text('txt1', null, ['class' => 'form-control']) !!}
              <br><br>
              {!! Form::label('icon', 'Seção 2') !!}
              <br>
              {!! Form::file('icon2') !!}
              <br>
              Descrição: {!! Form::text('txt2', null, ['class' => 'form-control']) !!}
              <br><br>
              {!! Form::label('icon', 'Seção 3') !!}
              <br>
              {!! Form::file('icon3') !!}
              <br>
              Descrição: {!! Form::text('txt3', null, ['class' => 'form-control']) !!}
              <br><br>
              <a href="{{url()->previous()}}" class="btn btn-primary">Voltar</a>
              {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
            </form>
          </div>
        </div>
      </div>
      <div class="col-6 col-md-6 col-lg-6">
        <div class="card dashboard-funil-full">
          <div class="card-header">
            <h4>
              <i class="far fa-star lga"></i> Ícones
            </h4>
          </div>
          <div class="card-body row">
              {!! Form::model($data, ['route' => 'adm.project.store', 'files' => true]) !!}
              {!! Form::label('icon', 'Seção 4') !!}
              <br>
              {!! Form::file('icon4') !!}
              <br>
              Descrição: {!! Form::text('txt4', null, ['class' => 'form-control']) !!}
              <br><br>
              {!! Form::label('icon', 'Seção 5') !!}
              <br>
              {!! Form::file('icon5') !!}
              <br>
              Descrição: {!! Form::text('txt5', null, ['class' => 'form-control']) !!}
              <br><br>
              {!! Form::label('icon', 'Seção 6') !!}
              <br>
              {!! Form::file('icon6') !!}
              <br>
              Descrição: {!! Form::text('txt6', null, ['class' => 'form-control']) !!}
              <br><br>
              {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>

@endsection
