@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header d-flex justify-content-between">
    <h1>Contatos</h1>
  </div>
  <div class="section-body">
    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card dashboard-funil-full">
          <table class="table table-responsive">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Form</th>
                <th scope="col">Nome</th>
                <th scope="col">Email</th>
                <th scope="col">Telefone</th>
                <th scope="col">Mensagem</th>
              </tr>
            </thead>
            <tbody>
              @foreach($data as $user)
              <tr>
                <th scope="row">{{$user->id}}</th>
                <td>{{$user->type}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->phone}}</td>
                <td>{{$user->msg}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</section>


@endsection
