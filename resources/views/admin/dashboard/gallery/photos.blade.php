@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header d-flex justify-content-between">
    <h1>Fotos - {{$album->title}}</h1>
  </div>
  <div class="section-body">
    <div class="card dashboard-funil-full">
    <div class="row m-4">
      <div class="col-md-3">
        {!! Form::open(['route' => ['adm.gallery.insertPhotos', $album->id], 'files' => true])!!}
          {!! Form::label('asd', 'Selecionar Fotos')!!}
          <br>
          {!! Form::file('photo[]', ['multiple' => true]) !!}
          <br><br>
          {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
        {!! Form::close() !!}
      </div>
      @foreach($data as $photo)
        <div class="col-md-3 ">

          <img src="{{url('/storage/gallery/'.$photo->photo)}}" class="cover-img rounded m-3" width="200px" height="200px">
          <a class="btn btn-danger btn-xs" href="{{ route('adm.gallery.deletePhotos', $photo->id) }}" onclick="return confirm('Deseja mesmo excluir?')">X</a>
        </div>
      @endforeach
      </div>
    </div>
  </div>

</section>


@endsection
