<section class="section">
  <div class="section-body">
    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card dashboard-funil-full">
          <div class="card-header">
            <h4>
              <i class="far fa-star lga"></i> Cadastrar
            </h4>
          </div>
          <div class="card-body row">
            @if(isset($album))
                {!! Form::model($album, ['route' => ['adm.gallery.update', $album->id], 'files' => true]) !!}
            @else
                {!! Form::open(['route' => 'adm.gallery.store', 'files' => true]) !!}
            @endif
              Título: {!! Form::text('title', null, ['class' => 'form-control', 'required']) !!}
              <br>
              Capa: {!! Form::file('cover') !!}
              <br>
              <br>
              {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>
