@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header d-flex justify-content-between">
    <h1>Albums</h1>
  </div>
  @include('admin.dashboard.gallery.form')
  <div class="section-body">
    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card dashboard-funil-full">
          <table class="table table-responsive">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Título</th>
                <th scope="col">Opções</th>
              </tr>
            </thead>
            <tbody>
              @foreach($data as $album)
              <tr>
                <th scope="row">{{$album->id}}</th>
                <td>{{$album->title}}</td>
                <td>
                  <a href="{{ route('adm.gallery.show', $album->id) }}">Editar</a> | 
                  <a href="{{ route('adm.gallery.photos', $album->id) }}">Fotos</a> | 
                  <a href="{{ route('adm.gallery.delete', $album->id)}} " onclick="return confirm('Deseja mesmo apagar?')">Apagar</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</section>


@endsection
