@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Localização</h1>
  </div>

  <div class="section-body">
    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card dashboard-funil-full">
          <div class="card-header">
            <h4>
              <i class="far fa-star lga"></i> Cadastrar
            </h4>
          </div>
          <div class="card-body row">
              {!! Form::model($data, ['route' => ['adm.locations.store', $data->id], 'files' => true]) !!}
              Título: {!! Form::text('title', null, ['class' => 'form-control', 'style' => 'width: 600px', 'required']) !!}
              <br>
              Card Título: {!! Form::text('card1', null, ['class' => 'form-control', 'style' => 'width: 600px', 'required']) !!}
              <br>
              Card sub-título: {!! Form::text('card2', null, ['class' => 'form-control', 'style' => 'width: 600px', 'required']) !!}
              <br>
              <br>
              <a href="{{url()->previous()}}" class="btn btn-primary">Voltar</a>
              {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>

@endsection
