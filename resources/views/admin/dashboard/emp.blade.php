@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Empreendimento</h1>
  </div>

  <div class="section-body">
    <div class="row">
      <div class="col-6 col-md-6 col-lg-6">
        <div class="card dashboard-funil-full">
          <div class="card-header">
            <h4>
              <i class="far fa-star lga"></i> Seção 1
            </h4>
          </div>
          <div class="card-body row">
              {!! Form::model($data, ['route' => ['adm.emp.store', $data->id], 'files' => true]) !!}
              Título: {!! Form::text('title1', null, ['class' => 'form-control', 'required']) !!}
              <br>
              Sub título: {!! Form::text('sub1', null, ['class' => 'form-control', 'required']) !!}
              <br>
              Text: {!! Form::textarea('text1', null, ['class' => 'form-control', 'required']) !!}
              <br>
              Imagem: {!! Form::file('img1') !!}
              <br><br>
              <a href="{{url()->previous()}}" class="btn btn-primary">Voltar</a>
              {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
            </form>
          </div>
        </div>
      </div>
      <div class="col-6 col-md-6 col-lg-6">
        <div class="card dashboard-funil-full">
          <div class="card-header">
            <h4>
              <i class="far fa-star lga"></i> Seção 2
            </h4>
          </div>
          <div class="card-body row">
              {!! Form::model($data, ['route' => ['adm.emp.store', $data->id], 'files' => true]) !!}
              Título: {!! Form::text('title2', null, ['class' => 'form-control', 'required']) !!}
              <br>
              Sub título: {!! Form::text('sub2', null, ['class' => 'form-control', 'required']) !!}
              <br>
              Text: {!! Form::textarea('text2', null, ['class' => 'form-control', 'required']) !!}
              <br>
              Imagem: {!! Form::file('img2') !!}
              <br><br>
              {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>

@endsection
