@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Plantas</h1>
  </div>

  <div class="section-body">
    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card dashboard-funil-full">
          <div class="card-header">
            <h4>
              <i class="far fa-star lga"></i> Títulos
            </h4>
          </div>
          <div class="card-body row">
              {!! Form::model($data, ['route' => ['adm.plants.store', $data->id], 'files' => true]) !!}
              Town House: {!! Form::text('txt1', null, ['class' => 'form-control', 'style' => 'width: 600px', 'required']) !!}
              <br>
              Apartamentos: {!! Form::text('txt2', null, ['class' => 'form-control', 'style' => 'width: 600px', 'required']) !!}
              <br>
              Coberturas: {!! Form::text('txt3', null, ['class' => 'form-control', 'style' => 'width: 600px', 'required']) !!}
              <br>
              Áreas Comuns: {!! Form::text('txt4', null, ['class' => 'form-control', 'style' => 'width: 600px', 'required']) !!}
              <br>
              <br>
              <a href="{{url()->previous()}}" class="btn btn-primary">Voltar</a>
              {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>

@endsection
