@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Início</h1>
  </div>

  <div class="section-body">
    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card dashboard-funil-full">
          <div class="card-header">
            <h4>
              <i class="far fa-star lga"></i> Cadastrar
            </h4>
          </div>
          <div class="card-body row">
              {!! Form::model($data, ['route' => ['adm.home.store', $data->id], 'files' => true]) !!}
              Título: {!! Form::text('title', null, ['class' => 'form-control', 'required']) !!}
              <br>
              Background: {!! Form::file('background') !!}
              <br>
              Url: {!! Form::text('url', null, ['class' => 'form-control', 'required']) !!}
              <br>
              <a href="{{url()->previous()}}" class="btn btn-primary">Voltar</a>
              {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>

@endsection
