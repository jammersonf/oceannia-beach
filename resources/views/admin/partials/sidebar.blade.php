<div class="main-sidebar sidebar-style-2">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand mb-2">
      <a href="{{ route('adm.panel') }}"><img src="{{ asset('/assets/img/brand.svg') }}" width="50"></a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="{{ route('adm.panel') }}"><img src="{{ asset('/assets/img/brand.svg') }}" width="50"></a>
    </div>
    <ul class="sidebar-menu">
      <li class="menu-header">{{ auth()->user()->name }}</li>
      <li class="menu-header">Módulos</li>
      <li>
        <a class="" href="{{ route('adm.home.create') }}" title="Leads">
        <i class=" far fa-star lga"></i>
        <span>Home</span>
      </a>
      </li>
      <li>
        <a class="" href="{{ route('adm.emp.create') }}" title="Leads">
        <i class=" far fa-star lga"></i>
        <span>Empreendimento</span></a>
      </li>
      <li class="dropdown ">
        <a href="#" class="nav-link has-dropdown">
          <i class=" far fa-star lga"></i>
          <span>Projeto</span>
        </a>
        <ul class="dropdown-menu" style="display: none;">
          <li>
            <a class=" nav-link" href="{{ route('adm.project.create') }}" title="Leads">Ícones</a>
          </li>
          <li>
            <a class=" nav-link" href="{{ route('adm.gallery.index') }}" title="Leads">Galeria</a>
          </li>
          <li>
            <a class=" nav-link" href="{{ route('adm.plants.create') }}" title="Leads">Plantas</a>
          </li>
        </ul>
      </li>
      <li>
        <a class="" href="{{ route('adm.locations.create') }}" title="Leads">
        <i class=" far fa-star lga"></i>
        <span>Localização</span></a>
      </li>
      <li>
        <a class="" href="{{ route('adm.contact.index') }}" title="Leads">
        <i class=" far fa-star lga"></i>
        <span>Contatos</span></a>
      </li>
    </ul>
  </aside>
</div>
