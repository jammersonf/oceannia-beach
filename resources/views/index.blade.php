<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>{{config('app.name')}}</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"
        integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('assets_fronts/styles/main.css')}}">
    <link rel="stylesheet" href="{{asset('assets_fronts/icon-fonts/css/style.css')}}">

    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"
        integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous">
    </script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"
        integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous">
    </script>

    <!-- Bootstrap font -->
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,300i,400,400i,800,800i&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
        integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
        crossorigin="" />
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
        integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
        crossorigin=""></script>

    @toastr_css
</head>

<body>

    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="text-center">

                <img class="mt-5 mb-2" src="{{url('/assets_fronts/imgs/logo.png')}}" alt="">
            </div>

            <ul class="list-unstyled components m-3">
                <li>
                    <a class="side-border text-menu" href="#empreendimento">Empreendimento</a>
                </li>
                <li>
                    <a class="side-border text-menu" href="#projetos">Projeto</a>
                </li>
                <li>
                    <a class="side-border text-menu" href="#localizacao">Localização</a>
                </li>
                <li>
                    <a class="side-border text-menu" href="#contato">Contato</a>
                </li>
            </ul>

            <ul class="list-unstyled CTAs">
                <li>
                    <a href="#" data-toggle="modal" data-target="#exampleModal"
                        class="article d-flex justify-content-start"><span
                            class="icon icon-tenho-interesse icon-2x px-3"></span>Tenho <br> interesse</a>
                </li>
                <li>
                    <a href="#" class="article d-flex justify-content-start"><span
                            class="icon icon-whats icon-2x  px-3"></span>Falar com <br> corretor</a>
                </li>
            </ul>
            <ul class="list-unstyled social">
            </ul>
            <ul class="list-unstyled bottom-list">
                <li>
                    <div class="d-flex justify-content-center mb-4">
                        <a href="#" class="btn btn-outline-secondary mx-2"><span class="icon icon-face px-1"></span></a>
                        <a href="#" class="btn btn-outline-secondary mx-2"><span class="icon icon-instagram"></span></a>
                    </div>
                </li>

                <li>
                    <div class="d-flex justify-content-center">
                        <p>(83)</p>
                        <h4>3247-0909</h4>
                    </div>

                </li>
                <li class="text-center">
                    <img class="m-4" src="{{url('/assets_fronts/imgs/logo-2.png')}}" alt="">
                </li>
            </ul>
        </nav>
        <nav class="d-md-none navbar fixed-top navbar-expand-lg navbar-light bg-light ">
            <div class="header-nav d-flex justify-content-between w-100">

                <a class="navbar-brand" href="#">
                    <img class="ml-3" src="{{url('/assets_fronts/imgs/logo-mobile.png')}}" alt="">
                    
                </a>
                <button class="navbar-toggler btn-line" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="list-unstyled components m-3 text-right">
                    <li class="my-4">
                        <a class="side-border text-menu text-gold" href="#empreendimento">Empreendimento</a>
                    </li>
                    <li class="my-4">
                        <a class="side-border text-menu text-gold" href="#projetos">Projeto</a>
                    </li>
                    <li class="my-4">
                        <a class="side-border text-menu text-gold" href="#localizacao">Localização</a>
                    </li>
                    <li class="my-4">
                        <a class="side-border text-menu text-gold" href="#contato">Contato</a>
                    </li>
                </ul>
                <ul class="list-unstyled">
                    <li class="d-flex justify-content-end">
                        <a href="#" data-toggle="modal" data-target="#exampleModal"
                            class="article text-gold p-3 "><span
                                class="icon icon-tenho-interesse icon-2x px-3 pt-1"></span><span class="link-mobile">Tenho interesse</span></a>
                    </li>
                </ul>
                <ul class="list-unstyled social">
                </ul>
                <ul class="list-unstyled bottom-list">
                    <li>
                        <div class="d-flex justify-content-end text-gold my-4">
                            <p class="pt-1 pr-1"><small>(83)</small></p>
                            <h4><small> 3247-0909</small></h4>
                        </div>
                    </li>

                    <li>
                        <div class="d-flex justify-content-end my-4">
                            <a href="#" class="btn btn-outline-white mx-2 py-2"><span class="icon icon-face px-1"></span></a>
                            <a href="#" class="btn btn-outline-white mx-2 py-2"><span class="icon icon-instagram"></span></a>
                        </div>
                    </li>
    
                </ul>
    
    
              {{-- <ul class="navbar-nav">
                <li class="nav-item active">
                  <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Features</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Pricing</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                </li>
              </ul> --}}
            </div>
          </nav>
        <!-- Page Content  -->
        <div id="content">
            {{-- index --}}
            <section style="
            background: linear-gradient(0deg, rgba(119, 119, 119, 0.8), rgba(122, 122, 122, 0.4)), url('{{url('/storage/home/'.$home1->background)}}');
            background-size: cover;
            background-position: center;
            " id="index-banner" class="content-container">
                <img src="{{url('/assets_fronts/imgs/square-leaf.png')}}" class="square-leaf-bl" alt="">
                <div class="row">
                    <div data-aos="fade-right" class="col-md-6">
                        <h1 class="text-gold margin-banner">
                            {{ $home1->title}}
                        </h1>
                    </div>
                    <div data-aos="fade-left" class="col-md-6 d-flex justify-content-center">
                        <a style="height: 100px; " class="align-middle margin-banner" href="#" data-toggle="modal" data-target="#modalIframe">
                            <div class="playBg">
                                <span class="icon icon-play play-size p-3 m-1 "></span>
                            </div>
                        </a>
                    </div>
                </div>

            </section>

            {{-- empreendimento section --}}
            <section id="empreendimento" class="content-container">
                <div class="row empreendimento-spacing">
                    <div data-aos="fade-right" class="col-md-6">
                        <div class="post-text">
                            <p class="text-section text-green-1">
                                {{ $home2->title1 }}
                            </p>
                            <p class="text-title text-green-2">
                                {{ $home2->sub1 }}
                            </p>
                            <p class="text-paragraph text-black">
                                {{ $home2->text1 }}
                            </p>
                        </div>
                    </div>
                    <div data-aos="fade-left" class="col-md-6">
                        <div class="border-offset">
                        <div class="row-img-holder" style="
                            background: url('{{url('/storage/home/'.$home2->img1)}}');
                            background-size: cover;
                            background-position: center;
                        ">
                            <div class="d-flex justify-content-center">

                                <a style="height: 100px; margin-top: 200px;" class="align-middle" href="#" data-toggle="modal" data-target="#modalIframeEmpreendimento">
                                    <div class="playBg">
                                        <span class="icon icon-play play-size p-3 m-1 "></span>
                                    </div>
                                </a>
                            </div>

                        </div>
                        </div>
                        
                    </div>
                </div>
                <div class="row empreendimento-spacing">
                    <div data-aos="fade-right" class="col-md-6">
                        <div class="border-offset ml-3">
                        <div  class="row-img-holder-right" style="
                            
                            background: url('{{url('/storage/home/'.$home2->img2)}}');
                            background-size: cover;
                            background-position: center;
                        "></div>
                        </div>

                    </div>

                    <div data-aos="fade-left" class="col-md-6">
                        <div class="post-text">
                            <p class="text-section text-green-1">
                                {{ $home2->title2 }}
                            </p>
                            <p class="text-title text-green-2">
                                {{ $home2->sub2 }}
                            </p>
                            <p class="text-paragraph text-black">
                                {{ $home2->text2 }}
                            </p>
                        </div>
                    </div>
                </div>
            </section>

            {{-- mid slider section --}}
            <section id="mid-slider" style="padding: 100px 0px;">
                <iframe data-aos="fade-right" src="https://cdn.flipsnack.com/widget/v2/widget.html?hash=ftpm2jjh6"
                    width="100%" height="480" seamless="seamless" scrolling="no" frameBorder="0"
                    allowFullScreen></iframe>

            </section>

            {{-- projetos section --}}
            <section id="projetos" class="content-container">
                <img src="{{url('/assets_fronts/imgs/square-leaf.png')}}" class="square-leaf-tr" alt="">

                <div class="row">
                    <div data-aos="fade-down" class="col-md-7">
                        <p class="text-section text-green-1">Projeto</p>
                        <p style="max-width: 500px;" class="text-title text-gold mb-5 pb-5">Lorem Ipsum is simply dummy
                            text of the printing and typesetting
                            industry
                        </p>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 my-3">
                        <button class="btn card-btn">
                            <img src="{{url('/storage/home/'.$project->icon1)}}" alt=""><br>
                            {{$project->txt1}}
                        </button>
                    </div>
                    <div class="col-md-4 my-3">
                        <button class="btn card-btn">
                            <img src="{{url('/storage/home/'.$project->icon2)}}" alt=""><br>
                            {{$project->txt2}}
                        </button>
                    </div>
                    <div class="col-md-4 my-3">
                        <button class="btn card-btn">
                            <img src="{{url('/storage/home/'.$project->icon3)}}" alt=""><br>
                            {{$project->txt3}}
                        </button>
                    </div>
                    <div class="col-md-4 my-3">
                        <button class="btn card-btn">
                            <img src="{{url('/storage/home/'.$project->icon4)}}" alt=""><br>
                            {{$project->txt4}}
                        </button>
                    </div>
                    <div class="col-md-4 my-3">
                        <button class="btn card-btn">
                            <img src="{{url('/storage/home/'.$project->icon5)}}" alt=""><br>
                            {{$project->txt5}}
                        </button>
                    </div>
                    <div class="col-md-4 my-3">
                        <button class="btn card-btn">
                            <img src="{{url('/storage/home/'.$project->icon6)}}" alt=""><br>
                            {{$project->txt6}}
                        </button>
                    </div>
                </div>

                <div class="row">
                    @foreach($albums as $key => $album)
                    @php $size = $key == 2 ? 12 : 6; @endphp
                    <div data-aos="fade-up" class="col-md-{{$size}} my-3">
                        <div style="
                            background: linear-gradient(180deg, rgba(0, 0, 0, 0.25) 8.94%, #1E201F 94.01%), url('{{url('/storage/gallery/'.$album->cover)}}');
                            background-size: cover;
                            background-position: center;
                            " class="projeto-card">
                            <p class="projeto-card-title text-title text-gold m-0">{{$album->title}}</p>
                            <button class="btn btn-primary-thick" data-toggle="modal"
                                data-target="#modal{{$album->id}}"><span class="icon icon-arrow-right  py-2 px-3 text-gold"></span></button>

                        </div>
                    </div>
                    @endforeach
                </div>
                <h1 data-aos="fade-right" class="display-3 text-gold text-center">PLANTAS</h1>
                <div data-aos="fade-down" class="text-center">

                    <ul class="nav nav-pills mb-3 justify-content-center " id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-town-tab" data-toggle="pill" href="#pills-town"
                                role="tab" aria-controls="pills-town" aria-selected="true">Town House</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-apartamentos-tab" data-toggle="pill"
                                href="#pills-apartamentos" role="tab" aria-controls="pills-apartamentos"
                                aria-selected="false">Apartamentos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-coberturas-tab" data-toggle="pill" href="#pills-coberturas"
                                role="tab" aria-controls="pills-coberturas" aria-selected="false">Coberturas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-comuns-tab" data-toggle="pill" href="#pills-comuns" role="tab"
                                aria-controls="pills-comuns" aria-selected="false">Áreas Comuns</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-town" role="tabpanel"
                            aria-labelledby="pills-town-tab">
                            <p class="text-paragraph text-gold py-4">
                                {{$plants->txt1}}
                            </p>
                            <div class="planta-img-holder">
                                <button class="btn btn-primary-thick text-gold text-title-normal px-3 btn-planta"
                                    data-toggle="modal" data-target="#modalPlanta" style="top: 7%; right: 17%">1</button>
                                <button class="btn btn-primary-thick text-gold text-title-normal px-3 btn-planta"
                                    data-toggle="modal" data-target="#modalPlanta" style="top: 7%; right: 50%">3</button>
                                <button class="btn btn-primary-thick text-gold text-title-normal px-3 btn-planta"
                                    data-toggle="modal" data-target="#modalPlanta" style="top: 7%; right: 84%">2</button>
                                <button class="btn btn-primary-thick text-gold text-title-normal px-3 btn-planta"
                                    data-toggle="modal" data-target="#modalPlanta" style="top: 40%; right: 84%">4</button>
                                <button class="btn btn-primary-thick text-gold text-title-normal px-3 btn-planta"
                                    data-toggle="modal" data-target="#modalPlanta" style="top: 73%; right: 84%">5</button>
                                <button class="btn btn-primary-thick text-gold text-title-normal px-3 btn-planta"
                                    data-toggle="modal" data-target="#modalPlanta" style="top: 73%; right: 50%">6</button>
                                <button class="btn btn-primary-thick text-gold text-title-normal px-3 btn-planta"
                                    data-toggle="modal" data-target="#modalPlanta" style="top: 73%; right: 17%">7</button>

                                <img class="img-fluid" src="{{url('/assets_fronts/imgs/planta.png')}}" alt="">
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-apartamentos" role="tabpanel"
                            aria-labelledby="pills-apartamentos-tab">
                            <p class="text-paragraph text-gold py-4">
                                {{$plants->txt2}}
                            </p>
                            <img class="img-fluid" src="{{url('/assets_fronts/imgs/planta.png')}}" alt="">
                        </div>
                        <div class="tab-pane fade" id="pills-coberturas" role="tabpanel"
                            aria-labelledby="pills-coberturas-tab">
                            <p class="text-paragraph text-gold py-4">
                                {{$plants->txt3}}
                            </p>
                            <img class="img-fluid" src="{{url('/assets_fronts/imgs/planta.png')}}" alt="">
                        </div>
                        <div class="tab-pane fade" id="pills-comuns" role="tabpanel" aria-labelledby="pills-comuns-tab">
                            <p class="text-paragraph text-gold py-4">
                                {{$plants->txt4}}
                            </p>
                            <img class="img-fluid" src="{{url('/assets_fronts/imgs/planta.png')}}" alt="">
                        </div>
                    </div>
                </div>

            </section>
            <section id="localizacao">
                <div style="max-width: 930px;" class="content-container pl-2">
                    <p class="text-section text-green-1">Localização</p>
                    <p class="text-title text-green-2">
                        {{$location->title}}
                    </p>
                </div>
                <div class="map-holder">
                    <div style="    z-index: 401;" class="text-center map-card-holder p-5">
                        <img class="map-card-place p-3 mb-5" src="{{url('/assets_fronts/imgs/place.png')}}" alt="">
                        <p class="text-paragraph text-gold mb-5">
                            {{$location->card1}}
                        </p>
                        <p class="text-title-bold text-gold">{{$location->card2}}</p>

                    </div>
                    <div style="position: relative; left: 70;" class="pb-5">{{-- margin-left: 120px; --}}
                        {{-- <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7917.241498266176!2d-34.88731330562973!3d-7.169763969463897!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7ace9ae72b93a15%3A0xdbf43f2977a9009!2sCondom%C3%ADnio%20Parque%20Jacum%C3%A3!5e0!3m2!1spt-BR!2sbr!4v1580704408085!5m2!1spt-BR!2sbr"
                            width="100%" height="500" frameborder="0" style="border:0;" allowfullscreen=""
                            class="mr-0"></iframe> --}}
                        <div style=" height: 500px; width: 150%" id="mapid"></div>
                    </div>
                </div>

            </section>

            <section id="contato">
                <img src="{{url('/assets_fronts/imgs/square-leaf.png')}}" class="square-leaf-tr-off" alt="">

                <div class="content-container container">
                    <div class="row mb-5 pb-4">
                        <div class="col-md-8">
                            <p class="text-section text-green-1">Contato</p>
                            <p class="text-title text-gold">Lorem Ipsum is simply dummy text of the printing and
                                typesetting industry</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            {!! Form::open(['route' => 'contact', 'class' => 'text-start ']) !!}
                            <div class="form-group my-2 float-container">
                                <label for="input-name">Nome:</label>
                                {!! Form::text('name', null, ['class' => 'form-control', 'data-placeholder' => 'Seu
                                Nome', 'id'=> 'input-name','name' =>
                                'name', 'required']) !!}
                            </div>
                            <div class="form-group  my-2 float-container">
                                <label for="input-email">Email:</label>
                                {!! Form::text('email', null, ['class' => 'form-control','data-placeholder' =>
                                'seu@email.com', 'id'=> 'input-email', 'required']) !!}
                            </div>
                            <div class="form-group   my-2 float-container">
                                <label for="input-phone">Telefone:</label>
                                {!! Form::text('phone', null, ['class' => 'form-control ','data-placeholder' => '(00)
                                00000-0000', 'id'=>
                                'input-phone', 'required']) !!}
                            </div>
                            <div class="form-group   mb-2 mt-4 mx-2">
                                <label for="input-mensagem">Mensagem:</label>
                                {!! Form::textarea('msg', null, ['class' => 'form-control ','id'=>
                                'input-mensagem', 'required']) !!}
                            </div>
                            <div class="d-none d-md-block text-start">
                                <button class="btn btn-primary btn-lg btn-block  my-3"
                                    type="submit">Enviar</strong></button>
                            </div>
                            </form>

                        </div>
                        <div class="col-md-4">
                            <div class="card-contact card-contact-spacing">
                                <h4 class="text-green-2 font-weight-bold">Tenho interesse</h4>
                                <p class="text-paragraph text-gold"><small>Fale com um dos nossos consultores.</small>
                                </p>
                                <button class="btn btn-outline-light" data-toggle="modal" data-target="#exampleModal">QUERO QUE ME LIGUE</button>
                            </div>
                            <div class="card-contact-light card-contact-spacing my-1">
                                <h4 class="text-green-1 font-weight-bold">Receba o Book Digital</h4>
                                <p class="text-paragraph text-green-2"><small>Lorem Ipsum is simply dummy text of the
                                        printing and typesetting industry.</small></p>
                                <button class="btn btn-outline-light" data-toggle="modal" data-target="#modalBook">QUERO
                                    MAIS INFORMAÇÕES</button>
                            </div>
                            <div class="card-contact card-contact-spacing">
                                <h4 class="text-green-2 font-weight-bold">Precisa de Ajuda ?</h4>
                                <p class="text-paragraph text-gold"><small>Fale com um dos nossos consultores.</small>
                                </p>
                                <button class="btn btn-outline-light">FALAR COM O CORRETOR</button>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="d-sm-none corretor-call-holder mx-3">
        <div class="corretor-call d-flex justify-content-center">
            <button class="btn btn-secondary-thick font-weight-bold m-3 btn-block">Falar com Corretor</button>
        </div>
    </div>

    {{-- modais --}}

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header d-flex justify-content-end">
                    <button type="button" data-dismiss="modal" aria-label="Close" class=" btn btn-primary-thick"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h1 class="text-white font-weight-bold">Tenho Interesse</h1>
                    <p class="text-paragraph text-green-2">Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry</p>
                    <div>
                        {!! Form::open(['route' => 'contact', 'class' => 'text-start ']) !!}
                        <div class="form-group my-2 float-container">
                            <label for="input-name">Nome:</label>
                            {{-- <input type="text" class="form-control " id="input-name" name="name" placeholder=""> --}}
                            {!! Form::text('name', null, ['class' => 'form-control', 'data-placeholder' => 'Seu Nome',
                            'id'=> 'input-name','name' =>
                            'name', 'required']) !!}
                        </div>
                        <div class="form-group  my-2 float-container">
                            <label for="input-email">Email:</label>
                            {{-- <input type="email" class="form-control" id="input-email"  name="email" placeholder=""> --}}
                            {!! Form::text('email', null, ['class' => 'form-control','data-placeholder' =>
                            'seu@email.com', 'id'=> 'input-email','name' =>
                            'email', 'required']) !!}
                        </div>
                        <div class="form-group   my-2 float-container">
                            <label for="input-phone">Whatsapp:</label>
                            {{-- <input type="text" class="phone form-control " id="input-phone" name="phone" placeholder=""> --}}
                            {!! Form::text('phone', null, ['class' => 'form-control ','data-placeholder' => '(00)
                            00000-0000', 'id'=>
                            'input-phone','name' => 'phone', 'required']) !!}
                        </div>
                        <div class="d-flex justify-content-center">

                            <button class="btn btn-primary btn-lg   my-3 px-5 py-4" type="submit">Tenho
                                Interesse</strong></button>
                        </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalBook" tabindex="-1" role="dialog" aria-labelledby="modalBookLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header d-flex justify-content-end">
                    <button type="button" data-dismiss="modal" aria-label="Close" class=" btn btn-primary-thick"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h1 class="text-white font-weight-bold">Receba o Book Digital</h1>
                    <p class="text-paragraph text-green-2">Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry</p>
                    <div>
                        {!! Form::open(['route' => 'contact', 'class' => 'text-start ']) !!}
                        <div class="form-group my-2 float-container">
                            <label for="input-name">Nome:</label>
                            {{-- <input type="text" class="form-control " id="input-name" name="name" placeholder=""> --}}
                            {!! Form::text('name', null, ['class' => 'form-control', 'data-placeholder' => 'Seu Nome',
                            'id'=> 'input-name','name' =>
                            'name', 'required']) !!}
                        </div>
                        <div class="form-group  my-2 float-container">
                            <label for="input-email">Email:</label>
                            {{-- <input type="email" class="form-control" id="input-email"  name="email" placeholder=""> --}}
                            {!! Form::text('email', null, ['class' => 'form-control','data-placeholder' =>
                            'seu@email.com', 'id'=> 'input-email','name' =>
                            'email', 'required']) !!}
                        </div>
                        <div class="d-flex justify-content-center">

                            <button class="btn btn-primary btn-lg   my-3 px-5 py-4" type="submit">Receba o
                                Book</strong></button>
                        </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
    @foreach($albums as $album)
    <div class="modal fade" id="modal{{$album->id}}" tabindex="-1" role="dialog" aria-labelledby="slider-modalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header d-flex justify-content-end">
                </div>
                <div class="modal-body">
                    <div class="d-flex justify-content-between">

                        <h1 class="text-white font-weight-bold">
                            {{$album->title}}
                        </h1>
                        <button type="button" data-dismiss="modal" aria-label="Close"
                            class=" btn btn-primary-thick"><span class="px-2" aria-hidden="true">&times;</span></button>
                    </div>

                    <div id="carouselExampleControls" class="carousel slide mt-2" data-ride="carousel">
                        <div class="row">
                            <div style="display: flex;
                                        align-items: center;" class="col-md-1">
                                <a href="#carouselExampleControls" role="button" data-slide="prev" class="btn btn-rounded ">
                                    <span class="icon icon-arrow-left-sm  text-gold"></span>
                                </a>
    
                            </div>
                            <div class="col-md-10">
                                <div class="carousel-inner">
                                    @foreach($album->photos as $key => $photo)
                                    @php $active = $key == 0 ? 'active' : '' @endphp
                                    <div class="carousel-item {{$active}}">
                                        <img src="{{url('/storage/gallery/'.$photo->photo)}}" class="d-block w-100 cover-img" width="100%" height="500px" alt="...">
                                    </div>
                                    @endforeach
                                </div>
        
                            </div>
                            <div style="display: flex;
                                        align-items: center;"  class="col-md-1">
                                <a href="#carouselExampleControls" role="button" data-slide="next" class="btn btn-rounded ">
                                    <span class="icon icon-arrow-right-sm  text-gold"></span>
                                </a>

        
                            </div>
                        </div>
                    </div>

                    <div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach

    <div class="modal fade" id="modalIframe" tabindex="-1" role="dialog" aria-labelledby="modalIframeLabel"
    aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header d-flex justify-content-end">
                    <button type="button" data-dismiss="modal" aria-label="Close" class=" btn btn-primary-thick"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <iframe width="100%" height="600px" src="{{$home1->url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalIframeEmpreendimento" tabindex="-1" role="dialog" aria-labelledby="modalIframeEmpreendimentoLabel"
    aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header d-flex justify-content-end">
                    <button type="button" data-dismiss="modal" aria-label="Close" class=" btn btn-primary-thick"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <iframe width="100%" height="600px" src="{{$home2->url1}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalPlanta" tabindex="-1" role="dialog" aria-labelledby="modalPlantaLabel"
    aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header d-flex justify-content-end">
                    <button type="button" data-dismiss="modal" aria-label="Close" class=" btn btn-primary-thick"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <img class="img-fluid" src="{{url('/assets_fronts/imgs/planta.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>


    @jquery
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"
        integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
        integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous">
    </script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar, #content').toggleClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });
    </script>
    @toastr_js
    @toastr_render
    {{-- labels dos inputs --}}
    <script>
        const FloatLabel = (() => {
  
  // add active class and placeholder 
  const handleFocus = (e) => {
    const target = e.target;
    target.parentNode.classList.add('active');
    target.setAttribute('placeholder', target.getAttribute('data-placeholder'));
  };
  
  // remove active class and placeholder
  const handleBlur = (e) => {
    const target = e.target;
    if(!target.value) {
      target.parentNode.classList.remove('active');
    }
    target.removeAttribute('placeholder');    
  };  
  
  // register events
  const bindEvents = (element) => {
    const floatField = element.querySelector('input');
    floatField.addEventListener('focus', handleFocus);
    floatField.addEventListener('blur', handleBlur);    
  };
  
  // get DOM elements
  const init = () => {
    const floatContainers = document.querySelectorAll('.float-container');
    
    floatContainers.forEach((element) => {
      if (element.querySelector('input').value) {
          element.classList.add('active');
      }      
      
      bindEvents(element);
    });
  };
  
  return {
    init: init
  };
})();

FloatLabel.init();

    </script>
    <script>
        var mymap = L.map('mapid').setView([51.505, -0.09], 13);
        L.tileLayer('https://api.mapbox.com/styles/v1/yusefrichard/ck6jhqjr31bkw1ipixk83oflw/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoieXVzZWZyaWNoYXJkIiwiYSI6ImNrNmpkcngwdTAzOXgza28zb2VndGU0MGMifQ.up9Rp20HlGBt73AULOILGA', {
            maxZoom: 18,
            id: 'mapbox/streets-v11',
            accessToken: 'your.mapbox.access.token'
        }).addTo(mymap);
        /* var LeafIcon = L.Icon.extend({
            options: {
                shadowUrl: {{url('/assets_fronts/imgs/marker.png')}},
                iconSize:     [38, 95],
                shadowSize:   [50, 64],
                iconAnchor:   [22, 94],
                shadowAnchor: [4, 62],
                popupAnchor:  [-3, -76]
            } 
        }); */
        /* var customIcon = new LeafIcon({iconUrl: {{url('/assets_fronts/imgs/marker.png')}}}); */

        var marker = L.marker([51.5, -0.09]).addTo(mymap);
    </script>
</body>

</html>
