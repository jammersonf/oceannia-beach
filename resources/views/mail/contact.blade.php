@component('mail::message')
<h4>Olá, {{$data->name}}</h4><br><br>

<p>Obrigado pelo seu interesse nos serviços da Oceannia. Em até 1 dia útil, nossa equipe entrará em contato.</p>

@endcomponent
