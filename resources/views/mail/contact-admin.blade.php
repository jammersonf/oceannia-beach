@component('mail::message')
<h4>Novo contato em Oceannia</h4><br><br>

Nome: <b>{{$data->name}}</b><br>
Email: <b>{{$data->email}}</b><br>
Telefone: <b>{{$data->phone}}</b><br>
Form: <b>{{$data->type}}</b><br>

@endcomponent
