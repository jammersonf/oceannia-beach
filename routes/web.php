<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::post('contact', 'HomeController@contact')->name('contact');

Auth::routes(['register' => false]);

Route::prefix('admin')->name('adm.')->group(function () {
    Route::get('/', 'AdminController@index')->name('panel');

    Route::get('inicio', 'AdminController@create')->name('home.create');
    Route::post('inicio', 'AdminController@store')->name('home.store');

    Route::get('emp', 'AdminController@empCreate')->name('emp.create');
    Route::post('emp', 'AdminController@empStore')->name('emp.store');

    Route::get('project', 'AdminController@projectCreate')->name('project.create');
    Route::post('project', 'AdminController@projectStore')->name('project.store');

    Route::get('plants', 'AdminController@plantCreate')->name('plants.create');
    Route::post('plants', 'AdminController@plantStore')->name('plants.store');

    Route::get('locations', 'AdminController@locationCreate')->name('locations.create');
    Route::post('locations', 'AdminController@locationStore')->name('locations.store');
    
    Route::get('contacts', 'AdminController@contacts')->name('contact.index');

    Route::prefix('galeria')->name('gallery.')->group(function() {
        Route::get('', 'GalleryController@index')->name('index');
        Route::get('create', 'GalleryController@create')->name('create');
        Route::post('create', 'GalleryController@store')->name('store');
        Route::get('{id}/photos', 'GalleryController@photos')->name('photos');
        Route::post('{id}/photos', 'GalleryController@insertPhotos')->name('insertPhotos');
        Route::get('{id}/photos/delete', 'GalleryController@deletePhotos')->name('deletePhotos');
        Route::get('{id}', 'GalleryController@show')->name('show');
        Route::post('{id}', 'GalleryController@update')->name('update');
        Route::get('{id}/delete', 'GalleryController@delete')->name('delete'); 
    });
});
