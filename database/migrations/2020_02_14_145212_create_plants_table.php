<?php

use App\Plant;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('txt1');
            $table->string('txt2');
            $table->string('txt3');
            $table->string('txt4');
            $table->timestamps();
        });

        Plant::create([
            'txt1' => '',
            'txt2' => '',
            'txt3' => '',
            'txt4' => '',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plants');
    }
}
