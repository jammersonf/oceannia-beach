<?php

use App\Home2;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHome2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home2', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title1');
            $table->string('sub1');
            $table->text('text1');
            $table->text('img1');
            $table->string('title2');
            $table->string('sub2');
            $table->text('text2');
            $table->text('img2');
            $table->timestamps();
        });

        Home2::create([
            'title1' => '',
            'sub1' => '',
            'text1' => '',
            'img1' => '',
            'title2' => '',
            'sub2' => '',
            'text2' => '',
            'img2' => '',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home2');
    }
}
