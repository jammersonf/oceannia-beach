<?php

use App\Project;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('icon1')->nullable();
            $table->string('txt1')->nullable();
            $table->string('icon2')->nullable();
            $table->string('txt2')->nullable();
            $table->string('icon3')->nullable();
            $table->string('txt3')->nullable();
            $table->string('icon4')->nullable();
            $table->string('txt4')->nullable();
            $table->string('icon5')->nullable();
            $table->string('txt5')->nullable();
            $table->string('icon6')->nullable();
            $table->string('txt6')->nullable();
            $table->timestamps();
        });

        Project::create([
            'icon1' => '',
            'txt1' => '',
            'icon2' => '',
            'txt2' => '',
            'icon3' => '',
            'txt3' => '',
            'icon4' => '',
            'txt4' => '',
            'icon5' => '',
            'txt5' => '',
            'icon6' => '',
            'txt6' => '',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
}
