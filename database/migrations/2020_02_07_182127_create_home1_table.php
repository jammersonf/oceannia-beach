<?php

use App\Home1;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHome1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home1', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('background');
            $table->text('url');
            $table->timestamps();
        });

        Home1::create([
            'title' => 'Título',
            'background' => '',
            'url' => ''
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home1');
    }
}
