<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contact';

    public $fillable = [
        'name',
        'email',
        'phone',
        'msg',
        'type'
    ];
}
