<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlbumPhoto extends Model
{
    protected $table = 'album_photos';

    public $fillable = [
        'album_id',
        'photo'
    ];
}
