<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Home1 extends Model
{
    protected $table = 'home1';

    public $fillable = ['title', 'background', 'url'];
}
