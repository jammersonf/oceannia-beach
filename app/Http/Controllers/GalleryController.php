<?php

namespace App\Http\Controllers;

use App\Album;
use App\AlbumPhoto;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    protected $album;
    protected $gallery;

    public function __construct(Album $album, AlbumPhoto $ab) {
        $this->middleware('auth');
        $this->album = $album;
        $this->gallery = $ab;
    }

    public function index(Request $request)
    {
        $data = $this->album->all();

        return view('admin.dashboard.gallery.index', compact('data'));
    }

    public function store(Request $request)
    {
        $input = $request->all();

        if (isset($input['cover'])) {
            $imageName = time().'.'.$input['cover']->extension();
            $input['cover']->move(public_path('storage/gallery'), $imageName);

            $input['cover'] = $imageName;
        } else {
            unset($input['cover']);
        }

        $this->album->create($input);

        toastr()->success('Cadastrado');

        return redirect()->route('adm.gallery.index');
    }

    public function show($id)
    {
        
        $data = $this->album->all();
        $album = $this->album->find($id);

        return view('admin.dashboard.gallery.index', compact('data', 'album'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();

        if (isset($input['cover'])) {
            $imageName = time().'.'.$input['cover']->extension();
            $input['cover']->move(public_path('storage/gallery'), $imageName);

            $input['cover'] = $imageName;
        } else {
            unset($input['cover']);
        }

        $this->album->find($id)->update($input);

        toastr()->success('atualizado');

        return redirect()->route('adm.gallery.index');
    }

    public function photos($id)
    {
        $album = $this->album->find($id);
        $data = $this->gallery->where('album_id', $id)->get();

        return view('admin.dashboard.gallery.photos', compact('data', 'album'));
    }

    public function insertPhotos($id, Request $request)
    {
        $album = $this->album->find($id);
        $input = $request->all();

        foreach($input['photo'] as $key => $photo) {
            $imageName = time().$key.'.'.$photo->extension();
            $photo->move(public_path('storage/gallery'), $imageName);

            $input['photo'] = $imageName;

            $album->photos()->create($input);
        }

        toastr()->success('Fotos cadastradas');

        return redirect()->route('adm.gallery.photos', $id);
    }

    public function deletePhotos($gallery)
    {
        $gallery = $this->gallery->find($gallery);
        $id = $gallery->album_id;

        $gallery->delete();
        
        toastr()->error('Foto apagada');

        return redirect()->route('adm.gallery.photos', $id);
    }
}
