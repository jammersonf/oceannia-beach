<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Home1;
use App\Home2;
use App\Location;
use App\Plant;
use App\Project;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index()
    {
        $messages = []; 

        return view('admin.dashboard.index', compact('messages'));
    }

    public function contacts()
    {
        $data = Contact::orderBy('id', 'desc')->get();

        return view('admin.dashboard.contacts', compact('data'));
    }

    public function create()
    {
        $data = Home1::find(1);

        return view('admin.dashboard.home', compact('data'));
    }

    public function store(Request $request)
    {
        $input = $request->all();
        if (isset($input['background'])) {
            $imageName = time().'.'.$input['background']->extension();
            $input['background']->move(public_path('storage/home'), $imageName);

            $input['background'] = $imageName;
        } else {
            unset($input['background']);
        }

        Home1::find(1)->update($input);

        toastr()->success('Cadastrado');

        return redirect()->route('adm.home.create');
    }

    public function plantCreate()
    {
        $data = Plant::find(1);

        return view('admin.dashboard.plants', compact('data'));
    }

    public function plantStore(Request $request)
    {
        $input = $request->all();

        Plant::find(1)->update($input);

        toastr()->success('Cadastrado');

        return redirect()->route('adm.plants.create');
    }

    public function locationCreate()
    {
        $data = Location::find(1);

        return view('admin.dashboard.locations', compact('data'));
    }

    public function locationStore(Request $request)
    {
        $input = $request->all();

        Location::find(1)->update($input);

        toastr()->success('Cadastrado');

        return redirect()->route('adm.locations.create');
    }

    public function empCreate()
    {
        $data = Home2::find(1);

        return view('admin.dashboard.emp', compact('data'));
    }

    public function empStore(Request $request)
    {
        $input = $request->all();

        if (isset($input['img1'])) {
            $imageName = time().'.'.$input['img1']->extension();
            $input['img1']->move(public_path('storage/home'), $imageName);

            $input['img1'] = $imageName;
        } else {
            unset($input['img1']);
        }

        if (isset($input['img2'])) {
            $imageName = time().'.'.$input['img2']->extension();
            $input['img2']->move(public_path('storage/home'), $imageName);

            $input['img2'] = $imageName;
        } else {
            unset($input['img2']);
        }

        Home2::find(1)->update($input);

        toastr()->success('Cadastrado');

        return redirect()->route('adm.emp.create');
    }

    public function projectCreate()
    {
        $data = Project::find(1);

        return view('admin.dashboard.project', compact('data'));
    }

    public function projectStore(Request $request)
    {
        $input = $request->all();

        if (isset($input['icon1'])) {
            $imageName = time().'.'.$input['icon1']->extension();
            $input['icon1']->move(public_path('storage/home'), $imageName);

            $input['icon1'] = $imageName;
        } else {
            unset($input['icon1']);
        }

        if (isset($input['icon2'])) {
            $imageName = time().'.'.$input['icon2']->extension();
            $input['icon2']->move(public_path('storage/home'), $imageName);

            $input['icon2'] = $imageName;
        } else {
            unset($input['icon2']);
        }

        if (isset($input['icon3'])) {
            $imageName = time().'.'.$input['icon3']->extension();
            $input['icon3']->move(public_path('storage/project'), $imageName);

            $input['icon3'] = $imageName;
        } else {
            unset($input['icon3']);
        }

        if (isset($input['icon4'])) {
            $imageName = time().'.'.$input['icon4']->extension();
            $input['icon4']->move(public_path('storage/project'), $imageName);

            $input['icon4'] = $imageName;
        } else {
            unset($input['icon4']);
        }

        if (isset($input['icon5'])) {
            $imageName = time().'.'.$input['icon5']->extension();
            $input['icon5']->move(public_path('storage/project'), $imageName);

            $input['icon5'] = $imageName;
        } else {
            unset($input['icon5']);
        }

        if (isset($input['icon6'])) {
            $imageName = time().'.'.$input['icon6']->extension();
            $input['icon6']->move(public_path('storage/project'), $imageName);

            $input['icon6'] = $imageName;
        } else {
            unset($input['icon6']);
        }

        Project::find(1)->update($input);

        toastr()->success('Cadastrado');

        return redirect()->route('adm.emp.create');
    }
}
