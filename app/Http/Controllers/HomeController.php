<?php

namespace App\Http\Controllers;

use App\Album;
use App\Contact;
use App\Home1;
use App\Home2;
use App\Location;
use App\Mail\AdminContact;
use App\Mail\MailContact;
use App\Plant;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function __construct() {
        //$this->middleware('guest');
    }

    public function index(Request $request)
    {
        $home1 = Home1::find(1);
        $home2 = Home2::find(1);
        $project = Project::find(1);
        $albums = Album::all();
        $plants = Plant::find(1);
        $location = Location::find(1);

        return view('index', compact('home1', 'home2', 'project', 'albums', 'plants', 'location'));
    }
    public function contact(Request $request)
    {
        $input = $request->all();
        if(isset($input['msg'])) {
            $input['type'] = 'contato';
        } else if(!isset($input['phone'])) {
            $input['type'] = 'book';
        } else if(!isset($input['msg'])) {
            $input['type'] = 'interesse';
        }
        
        $message = Contact::create($input);

        Mail::to('jammersonf@gmail.com')->send(new AdminContact($message));
        Mail::to($input['email'])->send(new MailContact($message));

        toastr()->success('Enviado');

        return redirect('/');
    }
}
