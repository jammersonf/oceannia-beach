<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'project';

    public $fillable = [
        'icon1',
        'txt1',
        'icon2',
        'txt2',
        'icon3',
        'txt3',
        'icon4',
        'txt4',
        'icon5',
        'txt5',
        'icon6',
        'txt6',
    ];
}
