<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Home2 extends Model
{
    protected $table = 'home2';

    public $fillable = [
        'title1',
        'sub1',
        'text1',
        'img1',
        'url1',
        'title2',
        'sub2',
        'text2',
        'img2',
    ];
}
