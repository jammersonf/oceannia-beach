<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $table = 'album';

    public $fillable = [
        'title',
        'cover'
    ];

    public function photos()
    {
        return $this->hasMany(AlbumPhoto::class, 'album_id');
    }
}
