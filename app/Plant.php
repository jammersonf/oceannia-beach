<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plant extends Model
{
    protected $table = 'plants';

    public $fillable = [
        'txt1',
        'txt2',
        'txt3',
        'txt4',
    ];
}
